<?php 
  include 'header.php'; 

?>
<title>JADWAL KSM</title>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <!-- <span><i class="fa fa-angle-right">&nbsp;</i>lala</span> -->
</div>
<!-- End of Page Heading -->


<!-- Content here -->

<div class="row justify-content-center"> 
  <div class="col-lg-12">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
       <center> <h6 class="m-0 font-weight-bold text-dark"style="font-family: 'Fira Sans', sans-serif;"> JADWAL KSM</h6>
      </div>  

      <div class="card-body" style="font-family: 'Fira Sans', sans-serif;">
      

      <!-- TABLE HERE -->
      <div class="table-responsive">
        <table class="table table-sm" id="dataTable" width="100%" cellspacing="0">

        <table class="table table-striped table-bordered table-hover">
  <thead>
  <tr style="text-align:center;" >
      <th  scope="col">No.</th>
      <th scope="col">Hari</th>
      <th width="250px" scope="col">Shift</th>
      <th scope="col">Kelas</th>
      <th   scope="col">Ruangan</th>
      <th width="250px" scope="col">Kode Matakuliah</th>
      <th width="300px"  scope="col">Nama Matakuliah</th>
      <th scope="col" colspan="2">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>SENIN</td>
      <td>09.30 - 11.30</td>
      <td>SI-40-01</td>
      <td>(B102) KU2.01.02</td>
      <td>ISH4L2</td>
      <td>ETIKA PROFESI, REGULASI TIK DAN BUDAYA INTERNASIONAL</td>
      <td ><a href="edit-jadwal.php"><button type="button" class="btn btn-primary">Edit</button></a> <td><a href="delete-jadwal.php"><button type="button" class="btn btn-danger">Delete</button></a></td></td>
      

    </tr>
    <tr>
      <th scope="row">2</th>
      <td>SABTU</td>
      <td>06:30 - 09:30</td>
      <td>SI-40-01</td>
      <td>(B304A) KU2.03.07</td>
      <td>HUH1G3</td>
      <td>PANCASILA DAN KEWARGANEGARAAN</td>
      <td ><a href="edit-jadwal.php"><button type="button" class="btn btn-primary">Edit</button></a> <td><a href="delete-jadwal.php"><button type="button" class="btn btn-danger">Delete</button></a></td></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>RABU</td>
      <td>14:30 - 17:30</td>
      <td>SI-41-01</td>
      <td>(B304A) KU2.03.07</td>
      <td>ISH2J3</td>
      <td>PENGEMBANGAN APLIKASI BERGERAK</td>
      <td ><a href="edit-jadwal.php"><button type="button" class="btn btn-primary">Edit</button></a> <td><a href="delete-jadwal.php"><button type="button" class="btn btn-danger">Delete</button></a></td></td>
    </tr>
  </tbody>
</table>
        </table>
      </div>
      <!-- END OF TABLE -->
      
      </div>
      
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>