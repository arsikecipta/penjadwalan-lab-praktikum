<?php 
  include 'header.php'; 
 
?>
<title>EDIT JADWAL</title>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <!-- <span><i class="fa fa-angle-right">&nbsp;</i>lala</span> -->
</div>
<!-- End of Page Heading -->

<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
       <center><h6 class="m-0 font-weight-bold text-dark" style="font-family: 'Fira Sans', sans-serif;"> EDIT JADWAL</h6></center> 
      </div>  

      <div class="card-body" style="font-family: 'Fira Sans', sans-serif;">
      <form>
  <div class="form-row">
    <div class="col">
    <b> <p>Hari </p></b>
      <input type="text" class="form-control border" value="SENIN"><br>
      <b> <p>Shift </p></b>
      <input type="text" class="form-control border"value="9.30-11.30" ><br>
      <b> <p>Kelas </p></b>
      <input type="text" class="form-control border"value="SI-40-01" ><br>
    </div>
    <div class="col">
    <b> <p>Ruangan </p></b>
      <input type="text" class="form-control border" value="(B102) KU2.01.02" ><br>
      <b> <p>Kode Matakuliah </p></b>
      <input type="text" class="form-control border" value="ISH4L2"><br>
      <b> <p>Nama Matakuliah </p></b>
      <input type="text" class="form-control border"value="ETIKA PROFESI, REGULASI TIK DAN BUDAYA INTERNASIONAL" ><br>
     
    </div>
  </div>
</form>
<center><button type="button" class="btn btn-primary btn-lg ">EDIT</button></center>
      <!-- TABLE HERE -->
      <!-- <div class="table-responsive">
        <table class="table table-sm" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th width="1%">No.</th>
              <th>Stirling Code</th>
              <th>Name</th>
              <th>Dimension</th>
              <th>Output (kVA)</th>
              <th>Price (Rp)</th>
              <th>Stock (Unit)</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          
           
          </tbody>
        </table>
      </div> -->
      <!-- END OF TABLE -->
      
      </div>
      
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>