<?php 
  include 'header.php'; 
 
?>
<title>Edit Profile</title>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <!-- <span><i class="fa fa-angle-right">&nbsp;</i>lala</span> -->
</div>
<!-- End of Page Heading -->

<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
       <center><h6 class="m-0 font-weight-bold text-dark" style="font-family: 'Fira Sans', sans-serif;"> EDIT PROFILE</h6></center> 
      </div>  

      <div class="card-body"style="font-family: 'Fira Sans', sans-serif;">
      <form>
  <div class="form-row">
    <div class="col">
    <b> <p>Email </p></b>
      <input type="email" class="form-control border"><br>
      <b> <p>Nama Lengkap </p></b>
      <input type="text" class="form-control border" ><br>
      <b> <p>Username </p></b>
      <input type="text" class="form-control border" ><br>
      <b> <p>Password </p></b>
      <input type="password" class="form-control border" ><br>
    </div>
    
  </div>
</form>
<center><button type="button" class="btn btn-primary btn-lg ">Edit</button></center>
     
      
      </div>
      
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>