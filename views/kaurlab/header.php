
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	

	<!-- Custom fonts for this template-->
	<link href="../../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css2?family=Fira+Sans&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Custom styles for this template-->
	<link href="../../assets/css/sb-admin-2-ram.css" rel="stylesheet">

	<!-- DATA TABLE CSS -->
	<link href="../../assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

    	<!-- Sidebar -->
    	<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    		<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="#"style="margin-top:100px;">
				<div class="sidebar-brand-icon rotate-n-15">
				</div>
				<!-- <div class="sidebar-brand-text mx-3"><img src="../../assets/img/logo.png" width="35%"></div> -->
			</a>

			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Nav Item - Dashboard -->
			<li class="nav-item ">
				<a class="nav-link" href="Dashboard.php">
				  <i class="fas fa-fw fa-home"></i>
				  <span style="font-family: 'Fira Sans', sans-serif;">Dashboard</span></a>
			</li>

		    <!-- Divider -->
		    <hr class="sidebar-divider">

		    <!-- Heading -->
		    <!-- <div class="sidebar-heading">
		        MAIN MENU
		    </div> -->

			<li class="nav-item ">
				<a class="nav-link" href="akun-laboran.php">
				  <i class="fas fa-user-alt"></i>
				  <span style="font-family: 'Fira Sans', sans-serif;">Akun Laboran</span></a>
			</li>
			<hr class="sidebar-divider">
			<li class="nav-item ">
				<a class="nav-link" href="jadwal-praktikum.php">
				  <i class="far fa-calendar-alt"></i>
				  <span style="font-family: 'Fira Sans', sans-serif;">Jadwal Praktikum</span>
				  <div class="btn-group">
				  <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 	</button>
						<div class="dropdown-menu dropdown-menu-right">
							<button class="dropdown-item" type="button">SI-41</button>
							<button class="dropdown-item" type="button">SI-42</button>
							<button class="dropdown-item" type="button">SI-43</button>
						</div>
					</div> 
				</a>
			</li>

		    <!-- Sidebar Toggler (Sidebar) -->
		    <div class="text-center d-none d-md-inline" style="margin-top:100px;">
		        <button class="square-circle border-0" id="sidebarToggle"></button> 
			
		    </div>

	    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
	    <div id="content-wrapper" class="d-flex flex-column" style="background-color:#84A98C;">

		    <!-- Main Content -->
		    <div id="content">

		        <!-- Topbar -->
		        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

			        
			        <!-- Sidebar Toggle (Topbar) -->
			        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
			        	<i class="fa fa-bars"></i>
			        </button>

			        <!-- Topbar Navbar -->
			        <ul class="navbar-nav ml-auto">
					<a class="nav-link" href="">
			            		<span class="mr-1 d-none d-lg-inline big"><i class="fas fa-user-circle" ></i></span>
			            	</a>

			            
						<div class="btn-group">
  <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

  </button>
  <div class="dropdown-menu dropdown-menu-right">
  <a href="edit-profile.php"><button class="dropdown-item" type="button"><i class="fas fa-user-edit">&nbspEdit profile</i></button></a>
	<a href="../home.php"><button class="dropdown-item" type="button"><i class="fas fa-sign-out-alt ">&nbspLogout</i></button></a>
  </div>
</div>
		        	</ul>

				</nav>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">